export const workType = [
    {
        id: '1',
        description: 'P - Workorder'
    },
    {
        id: '2',
        description: 'P - Time Based'
    }
];

export const equipmentCode = [
    { id: "1",  description: "8.A.0 - Test Drive - General" },
    { id: "2",  description: "8.A.1 - Test Drive - Washpipe" },
    { id: "3",  description: "8.A.2 - Test Drive - Quil" },
    { id: "4",  description: "8.A.3 - Test Drive - Hydralic" },
    { id: "5",  description: "8.A.0 - Test Drive - General" },
    { id: "6",  description: "8.A.1 - Test Drive - Washpipe" },
    { id: "7",  description: "8.A.2 - Test Drive - Quil" },
    { id: "8",  description: "8.A.3 - Test Drive - Hydralic" }
];

export const inventoryParts = [
    {
        id: 1,
        itemName: 'item1',
        itemDescription: 'Washpipe. Canring AC Top Drive 6027',
        code: '8.A.1',
        codeDescription: 'Top Drive - Wahpipe',
        quantity: 5,
        requriredQty: 0
    },
    {
        id: 2,
        itemName: 'Item2',
        itemDescription: 'Fastner, Washpipe 7 3/8"',
        code: '8.A.1',
        codeDescription: 'Top Drive - Wahpipe',
        quantity: 5,
        requriredQty: 0
    },
    {
        id: 3,
        itemName: 'Item3',
        itemDescription: 'Liner 6.5",TSC Mud Pump Consumables',
        code: '8.B.1',
        codeDescription: 'Mud Pump - Consumaables',
        quantity: 9,
        requriredQty: 0
    },
    {
        id: 4,
        itemName: 'Item4',
        itemDescription: 'Piston 6.5",TSC Mud Pump Consumables',
        code: '8.B.1',
        codeDescription: 'Mud Pump - Consumaables',
        quantity: 6,
        requriredQty: 0
    },
    {
        id: 5,
        itemName: 'Item5',
        itemDescription: 'Liner 6.5", TSC Mud Pump Dampners',
        code: '8.B.2',
        codeDescription: 'Mud Pump - Dampners',
        quantity: 2,
        requriredQty: 0
    },
    {
        id: 6,
        itemName: 'Item6',
        itemDescription: 'Gear Oil, CVX Ultra Lite Oil (Gal)',
        code: '8.MRO0',
        codeDescription: 'MRO - General',
        quantity: 500,
        requriredQty: 0
    },
    {
        id: 7,
        itemName: 'Item7',
        itemDescription: 'Pipe Dope, TSC Copper Coat Hi-Temp',
        code: '8.MRO0',
        codeDescription: 'MRO - General',
        quantity: 20,
        requriredQty: 0
    },
    {
        id: 8,
        itemName: 'Item8',
        itemDescription: 'Ethernet Cabel - CAT5e 40ft',
        code: '8.MRO0',
        codeDescription: 'MRO - General',
        quantity: 10,
        requriredQty: 0
    }
];

export const maintenanceLog = [
    {
        id: 1,
        createdDate: '08/10/18',
        eventDate: '08/09/18',
        workType: 'P - Workorder',
        equipmentCode: '8.G - Engines',
        downTime: [
            {
                id: 1,
                totalRepairRime: 0.0,
                daignosisByRig: 0.0,
                waitingOnTech: 0.0,
                daignosisByTech: 0.0,
                waitingOnParts: 0.0,
                repair: 0.0
            },
            {
                id: 2,
                totalRepairRime: 0.0,
                daignosisByRig: 0.0,
                waitingOnTech: 0.0,
                daignosisByTech: 0.0,
                waitingOnParts: 0.0,
                repair: 0.0
            },
        ],
        inventory: [
            {
                id: 1,
                itemName: 'item1',
                itemDescription: 'Washpipe. Canring AC Top Drive 6027',
                code: '8.A.1',
                codeDescription: 'Top Drive - Wahpipe',
                quantity: 5,
                requriredQty: 0
            },
            {
                id: 2,
                itemName: 'Item2',
                itemDescription: 'Fastner, Washpipe 7 3/8"',
                code: '8.A.1',
                codeDescription: 'Top Drive - Wahpipe',
                quantity: 5,
                requriredQty: 0
            },
            {
                id: 3,
                itemName: 'Item3',
                itemDescription: 'Liner 6.5",TSC Mud Pump Consumables',
                code: '8.B.1',
                codeDescription: 'Mud Pump - Consumaables',
                quantity: 9,
                requriredQty: 0
            },
            {
                id: 4,
                itemName: 'Item4',
                itemDescription: 'Piston 6.5",TSC Mud Pump Consumables',
                code: '8.B.1',
                codeDescription: 'Mud Pump - Consumaables',
                quantity: 6,
                requriredQty: 0
            },
            {
                id: 5,
                itemName: 'Item5',
                itemDescription: 'Liner 6.5", TSC Mud Pump Dampners',
                code: '8.B.2',
                codeDescription: 'Mud Pump - Dampners',
                quantity: 2,
                requriredQty: 0
            },
            {
                id: 6,
                itemName: 'Item6',
                itemDescription: 'Gear Oil, CVX Ultra Lite Oil (Gal)',
                code: '8.MRO0',
                codeDescription: 'MRO - General',
                quantity: 500,
                requriredQty: 0
            },
            {
                id: 7,
                itemName: 'Item7',
                itemDescription: 'Pipe Dope, TSC Copper Coat Hi-Temp',
                code: '8.MRO0',
                codeDescription: 'MRO - General',
                quantity: 20,
                requriredQty: 0
            },
            {
                id: 8,
                itemName: 'Item8',
                itemDescription: 'Ethernet Cabel - CAT5e 40ft',
                code: '8.MRO0',
                codeDescription: 'MRO - General',
                quantity: 10,
                requriredQty: 0
            }
        ],

    }
];

export const maintenanceEntries =  [
    
];
