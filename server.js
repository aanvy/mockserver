import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import  httpProxy from 'http-proxy';

const apiProxy = httpProxy.createProxyServer();
const apiServer = 'http://localhost:8082';

const SERVER_PORT = 80;

const app =  express();

app.all("/api/*", function(req, res) {
    apiProxy.web(req, res, {target: apiServer});
});

// set up a route to redirect http to https
app.use(bodyParser.json()); // support json encoded bodies
// Redirect HTTP to HTTPS for all non-dev environments
app.use('/index.html', express.static(path.join(__dirname, 'static')));

function requireHTTPS(req, res, next) {
    if(!(req.headers['x-forwarded-proto'] && req.headers['x-forwarded-proto'] === "https") && !req.secure) {
      res.setHeader('Cache-Control', 'public, max-age=86400, must-revalidate');
      return res.redirect(301, 'https://' + req.get('host') + req.url);
    }
    next();
}

//app.use(requireHTTPS);
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


//Enable CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

app.use('/app/', express.static(path.join(__dirname, 'app')));
app.use('/', express.static(path.join(__dirname, 'static')));



app.set('port', SERVER_PORT);
const http = require('http');

const server = http.createServer(app);
server.listen(SERVER_PORT, () => console.log(`API running on localhost:${SERVER_PORT}`));
